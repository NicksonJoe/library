<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;

class BookController extends Controller
{
    public function index()
    {

    }

    public function show(Book $book)
    {
        return view('profile.book.show', ['book' => $book]);
    }
}
