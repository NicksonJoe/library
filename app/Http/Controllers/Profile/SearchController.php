<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Services\SearchService;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $search = new SearchService();
        $books = $search->search($request);

        return view('profile.search.index', ['books' => $books]);
    }
}
