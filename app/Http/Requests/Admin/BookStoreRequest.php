<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class BookStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'categories' => ['required', 'array', 'exists:categories,id'],
            'title' => ['required', 'string', 'max:255'],
            'description' => ['string'],
            'publishing_house' => ['required', 'string', 'max:255'],
            'year_publishing' => ['required','digits:4','integer','min:1900','max:'.(date('Y')+1)],
            'isbn' => ['required', 'string', 'max:255'],
            'image' => ['required', 'image:jpeg,png,jpg'],
        ];
    }
}
