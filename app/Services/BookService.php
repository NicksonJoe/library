<?php
declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\Admin\BookStoreRequest;
use App\Http\Requests\Admin\BookUpdateRequest;
use App\Models\Book;

class BookService
{
    public function create(BookStoreRequest $request): Book
    {
        \DB::beginTransaction();

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('books', 'public');
        }

        $book = Book::create([
            'title' => $request->title,
            'description' => $request->description ?? null,
            'publishing_house' => $request->publishing_house,
            'year_publishing' => $request->year_publishing,
            'isbn' => $request->isbn,
            'image' => $path ?? null,
        ]);

        if ($request->has('categories')) {
            $book->categories()->attach($request->categories);
        }

        if ($request->has('authors')) {
            $book->authors()->attach($request->authors);
        }

        \DB::commit();

        return $book;
    }

    public function update(BookUpdateRequest $request, Book $book): Book
    {
        \DB::beginTransaction();

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('books', 'public');
        } else {
            $path = $book->image;
        }

        $book->update([
            'title' => $request->title,
            'description' => $request->description ?? null,
            'publishing_house' => $request->publishing_house,
            'year_publishing' => $request->year_publishing,
            'isbn' => $request->isbn,
            'image' => $path ?? null,
        ]);

        if ($request->has('categories')) {
            $book->categories()->sync($request->categories);
        }

        if ($request->has('authors')) {
            $book->authors()->sync($request->authors);
        }

        \DB::commit();

        return $book;
    }

    public function remove(Book $book): void
    {
        $book->categories()->detach();
        $book->delete();
    }
}
