<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\Book;
use Illuminate\Http\Request;

class SearchService
{
    public function search(Request $request)
    {
        $querySearch = $request->input('search');
        if (!$querySearch) {
            return [];
        }

        $books = Book::where('status', 'active')
                ->where(function ($query) use ($querySearch) {
                    $query->where('title', 'like', '%'.$querySearch.'%')
                        ->orWhere('description', 'like', '%'.$querySearch.'%')
                        ->orWhere('isbn', 'like', '%'.$querySearch.'%')
                        ->orWhere('year_publishing', 'like', '%'.$querySearch.'%')
                        ->orWhereHas('categories', function ($queryCategory) use ($querySearch) {
                            $queryCategory->where('name', 'like', '%'.$querySearch.'%');
                        })
                        ->orWhereHas('authors', function ($queryAuthor) use ($querySearch) {
                            $queryAuthor->where('name', 'like', '%'.$querySearch.'%');
                        });
                })
                ->with('categories')
                ->get();

        return $books;
    }
}
