<?php
declare(strict_types=1);

namespace App\Services;

use App\Http\Requests\Admin\UserUpdateRequest;
use App\Http\Requests\Profile\ProfileRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function update(UserUpdateRequest $request, User $user): void
    {
        $user->update([
            'role_id' => $request->role,
            'name' => $request->name,
            'email' => $request->email,
        ]);
    }

    public function updateProfile(ProfileRequest $request, User $user): void
    {
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
    }

    public function remove(User $user): void
    {
        $user->delete();
    }
}
