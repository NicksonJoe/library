<?php

namespace Database\Seeders;

use App\Models\Author;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $author = new Author();
        $author->name = 'Лев Толстой';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->name = 'Антон Чехов';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->name = 'Николай Гоголь';
        $author->status = 'active';
        $author->save();

        $author = new Author();
        $author->name = 'Михаил Булгаков';
        $author->status = 'active';
        $author->save();
    }
}
