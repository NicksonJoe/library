<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $category = new Category;
        $category->name = 'Детективы';
        $category->status = 'active';
        $category->save();

        $category = new Category;
        $category->name = 'Фантастика';
        $category->status = 'active';
        $category->save();

        $category = new Category;
        $category->name = 'Классика';
        $category->status = 'active';
        $category->save();
    }
}
