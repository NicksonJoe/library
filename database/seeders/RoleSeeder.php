<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new Role;
        $user->name = 'admin';
        $user->status = 'active';
        $user->save();

        $user = new Role;
        $user->name = 'user';
        $user->status = 'active';
        $user->save();
    }
}
