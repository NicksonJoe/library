@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')


        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Книга</div>

                    <div class="card-body">

                        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.books.store') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="authors" class="col-md-4 col-form-label text-md-end">Автор</label>

                                <div class="col-md-6">
                                    <select name="authors[]" id="authors" class="form-control @error('authors') is-invalid @enderror" multiple required>
                                        @forelse ($authors as $author)
                                            <option value="{{ $author->id }}" >{{ $author->name }}</option>
                                        @empty
                                            <option selected disabled>Пусто</option>
                                        @endforelse
                                    </select>

                                    @error('authors')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="categories" class="col-md-4 col-form-label text-md-end">Категория</label>

                                <div class="col-md-6">
                                    <select name="categories[]" id="categories" class="form-control @error('categories') is-invalid @enderror" multiple required>
                                        @forelse ($categories as $category)
                                            <option value="{{ $category->id }}" >{{ $category->name }}</option>
                                        @empty
                                            <option selected disabled>Пусто</option>
                                        @endforelse
                                    </select>

                                    @error('categories')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Название</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('title') is-invalid @enderror" name="title" required>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Описание</label>

                                <div class="col-md-6">
                                    <textarea id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" required></textarea>

                                    @error('description')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">Издательство</label>

                                <div class="col-md-6">
                                    <input id="publishing_house" type="text" class="form-control @error('publishing_house') is-invalid @enderror" name="publishing_house" required>

                                    @error('publishing_house')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="year_publishing" class="col-md-4 col-form-label text-md-end">Год издания</label>

                                <div class="col-md-6">
                                    <input id="year_publishing" type="number"  class="form-control @error('year_publishing') is-invalid @enderror" name="year_publishing" required>

                                    @error('year_publishing')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="isbn" class="col-md-4 col-form-label text-md-end">ISBN</label>

                                <div class="col-md-6">
                                    <input id="isbn" type="text" class="form-control @error('isbn') is-invalid @enderror" name="isbn" required>
                                    @error('isbn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                            </div>
                            <div class="row mb-3">
                                <label for="image" class="col-md-4 col-form-label text-md-end">Обложка</label>

                                <div class="col-md-6">
                                    <input id="image" type="file" class="form-control @error('image') is-invalid @enderror" name="image" required>

                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Создать
                                    </button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
