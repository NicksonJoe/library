@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')


        <a href="{{ route('admin.books.create') }}" class="btn btn-success mb-3">Создать</a>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#id</th>
                    <th scope="col">Название</th>
                    <th scope="col">Издательство</th>
                    <th scope="col">Год издания</th>
                    <th scope="col">ISBN</th>
                    <th scope="col">Действия</th>
                </tr>
            </thead>
            <tbody>
                @forelse($books as $book)
                    <tr>
                        <th scope="row">{{ $book->id }}</th>
                        <td>{{ $book->title }}</td>
                        <td>{{ $book->publishing_house }}</td>
                        <td>{{ $book->year_publishing }}</td>
                        <td>{{ $book->isbn }}</td>
                        <td>
                            <a href="{{ route('admin.books.show', $book->id) }}" class="btn btn-success">Просмотр</a>
                            <a href="{{ route('admin.books.edit', $book->id) }}" class="btn btn-primary">Редактирование</a>
                            <form action="{{ route('admin.books.destroy', $book->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <p>Пусто</p>
                @endforelse
            </tbody>
        </table>

    </div>

@endsection
