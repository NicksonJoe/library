@extends('layouts.app')

@section('content')

    <div class="container">

        @include('admin.components.menu')


        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Книга</div>

                    <div class="card-body">

                        <div class="row mb-3">
                            <label for="categories" class="col-md-4 col-form-label text-md-end">Автор</label>

                            <div class="col-md-6">
                                <select class="form-control" disabled multiple>
                                    @forelse ($book->authors as $author)
                                        <option value="{{ $author->id }}" >{{ $author->name }}</option>
                                    @empty
                                        <option selected disabled>Пусто</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="categories" class="col-md-4 col-form-label text-md-end">Категория</label>

                            <div class="col-md-6">
                                <select class="form-control" disabled multiple>
                                    @forelse ($book->categories as $category)
                                        <option value="{{ $category->id }}" >{{ $category->name }}</option>
                                    @empty
                                        <option selected disabled>Пусто</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Название</label>

                            <div class="col-md-6">
                                <input id="name" type="text" disabled class="form-control" name="title" value="{{ $book->title }}" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Описание</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" disabled name="description" required>{{ $book->description }}</textarea>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">Издательство</label>

                            <div class="col-md-6">
                                <input id="publishing_house" type="text" class="form-control" disabled value="{{ $book->publishing_house }}" name="publishing_house" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="year_publishing" class="col-md-4 col-form-label text-md-end">Год издания</label>

                            <div class="col-md-6">
                                <input id="year_publishing" type="number"  class="form-control" disabled value="{{ $book->year_publishing }}" name="year_publishing" required>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="isbn" class="col-md-4 col-form-label text-md-end">ISBN</label>

                            <div class="col-md-6">
                                <input id="isbn" type="text" class="form-control" disabled value="{{ $book->isbn }}" name="isbn" required>
                            </div>

                        </div>
                        <div class="row mb-3">
                            <label for="image" class="col-md-4 col-form-label text-md-end">Обложка</label>

                            <div class="col-md-6">
                                <img src="/storage/{{ $book->image }}" width="200px" height="200px" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
