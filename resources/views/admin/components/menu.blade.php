<div class="nav-scroller py-1 mb-2">
    <nav class="nav d-flex">
        <a class="p-2 text-muted" href="{{ route('admin.users.index') }}">Список пользователей</a>
        <a class="p-2 text-muted" href="{{ route('admin.books.index') }}">Список книг</a>
    </nav>
</div>
