<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>Blog Template · Bootstrap v4.6</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/blog/">


    <!-- Bootstrap core CSS -->
    <link href="/assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair&#43;Display:700,900" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="/assets/blog.css" rel="stylesheet">
{{--    @vite(['resources/assets/dist/css/bootstrap.min.css', 'resources/css/blog.css'])--}}

</head>
<body>

    <div class="container">
        <header class="blog-header py-3 mb-3">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4 pt-1">

                    <a class="text-muted" href="{{ route('profile.search') }}" aria-label="Search">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none" stroke="currentColor"
                             stroke-linecap="round" stroke-linejoin="round" stroke-width="2" class="mx-3" role="img"
                             viewBox="0 0 24 24" focusable="false"><title>Search</title>
                            <circle cx="10.5" cy="10.5" r="7.5"/>
                            <path d="M21 21l-5.2-5.2"/>
                        </svg>
                    </a>

                </div>
                <div class="col-4 text-center">
                    <a class="blog-header-logo text-dark" href="/">Library</a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                                  @guest
                        @if (Route::has('login'))
                            <a class="btn btn-sm btn-outline-secondary" href="{{ route('login') }}">{{ __('Login') }}</a>
                        @endif

                        @if (Route::has('register'))
                            <a class="btn btn-sm btn-outline-secondary" href="{{ route('register') }}">{{ __('Register') }}</a>
                        @endif
                    @else

                        <a href="{{ route('profile.home') }}" class="nav-link">
                            {{ Auth::user()->name }}
                        </a>

                        <a class="btn btn-sm btn-outline-secondary" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>

                    @endguest


                </div>
            </div>
        </header>
    </div>

    <main role="main" class="mb-2">

        @yield('content')

    </main><!-- /.container -->

    <footer class="blog-footer">

        <p>
            <a href="#">Back to top</a>
        </p>
    </footer>

</body>
</html>
