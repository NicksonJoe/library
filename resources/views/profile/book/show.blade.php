@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row mb-2">

            <div class="col-md-6">
                <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm position-relative">
                    <div class="col p-4 d-flex flex-column position-static">
                        <strong class="d-inline-block mb-2 text-primary">
                            Категории:
                            @foreach($book->categories as $category)
                                <span class="badge badge-primary">{{ $category->name }}</span>
                            @endforeach
                        </strong>
                        <strong class="d-inline-block mb-2 text-primary">
                            Авторы:
                            @foreach($book->authors as $author)
                                <span class="badge badge-primary">{{ $author->name }}</span>
                            @endforeach
                        </strong>
                        <h3 class="mb-0">{{ $book->title }}</h3>
                        <div class="mb-1 text-muted">year: {{ $book->year_publishing }}</div>
                        <div class="mb-1 text-muted">isbn: {{ $book->isbn }}</div>
                        <div>
                            {{ $book->description }}
                        </div>
                    </div>
                    <div class="col-auto d-none d-lg-block">
                        <img src="/storage/{{ $book->image }}" class="bd-placeholder-img" width="200" height="250">
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
