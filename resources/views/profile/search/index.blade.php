@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row mb-2">
            <div class="col-lg-6 offset-lg-3">
                <form class="card p-2" method="GET" action="{{ route('profile.search') }}">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Поиск" value="{{ $_GET['search'] }}">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Поиск</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row mb-2">

            @forelse($books as $book)
                @include('profile.components.book', $book)
            @empty
                <p>Пусто</p>
            @endforelse

        </div>
    </div>

@endsection
