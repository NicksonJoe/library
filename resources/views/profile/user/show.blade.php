@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="nav-scroller py-1 mb-2">
            <nav class="nav d-flex justify-content-between">
                <a class="p-2 text-muted" href="{{ route('profile.user.edit') }}">Изменить профиль</a>
            </nav>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Профиль</div>

                    <div class="card-body">
                        <div class="row mb-3">
                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" disabled class="form-control" name="name" value="{{ $user->name }}">
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" disabled class="form-control" name="email" value="{{ $user->email }}">
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

@endsection
