<?php

use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth')->name('home');

Route::prefix('admin')
    ->as('admin.')
    ->middleware(['auth', 'account:admin'])
    ->group(function () {

        Route::get('/', [App\Http\Controllers\Admin\MainController::class, 'index'])->name('home');

        Route::resource('users', App\Http\Controllers\Admin\UserController::class)->only([
            'index', 'show', 'edit', 'update', 'destroy'
        ]);
        Route::resource('books', App\Http\Controllers\Admin\BookController::class)->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]);

});

Route::prefix('profile')
    ->as('profile.')
    ->middleware(['auth', 'account:user'])
    ->group(function () {

        Route::get('/', [App\Http\Controllers\Profile\ProfileController::class, 'index'])->name('home');

        Route::get('/search', [App\Http\Controllers\Profile\SearchController::class, 'index'])->name('search');

        Route::prefix('my')->as('user.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\ProfileController::class, 'show'])->name('show');
            Route::get('/edit', [App\Http\Controllers\Profile\ProfileController::class, 'edit'])->name('edit');
            Route::post('/update', [App\Http\Controllers\Profile\ProfileController::class, 'update'])->name('update');
        });

        Route::prefix('books')->as('books.')->group(function () {
            Route::get('/', [App\Http\Controllers\Profile\BookController::class, 'index'])->name('index');
            Route::get('{book}', [App\Http\Controllers\Profile\BookController::class, 'show'])->name('show');
        });
});





